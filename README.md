（新开发wangmarket的插件、或者对wangmarket进行扩展开发、定制时，可以 fork 本项目。在本项目基础上，直接进行开发。  
这里提供一个基于此做的一个万能表单插件 https://gitee.com/leimingyun/wangmarket_plugin_form ，其 readme.md 的描述内容、以及代码、目录结构，以供参考）


## 功能介绍
[wangmarket（网市场云建站系统）](https://gitee.com/mail_osc/wangmarket) 的 插件开发示例Demo 插件。  
它可以给你一个简单的参考，比如controller放到哪、静态资源放到哪、jsp页面放到哪、目录结构等，按照统一的插件约定进行开发。开发出来的插件可以非常方便的打包为jar，随便放到任何网市场云建站系统中就可以使用。  
当然你也可以用这种方式，来进行你自己的扩展开发、定制。因为wangmarket项目本身是以jar的形式存在，所以你及时进行了扩展开发，也不影响我们wangmarket的后续版本升级。因为升级后你只需要换上最新版本的 wangmarket-xxx.jar 就可以了！  

## 使用条件
1. 本项目(生成的 target/wangmarket-plugin-xxx.jar)放到 [网市场云建站系统](https://gitee.com/leimingyun/wangmarket_deploy) 中才可运行使用
1. 网市场云建站系统本身需要 v5.3 或以上版本。
1. 本项目只支持 mysql 数据库。使用默认 sqlite 数据库的不可用（需要标注一下支持哪种数据库。如果是这个插件涉及到数据表、以及数据表字段方面的改动，那就只支持mysql数据库。如果没有数据表及字段属性的改动，那就支持 mysql、sqlite数据库）

## 使用方式
1. 下载 /target/ 中的jar包
1. 将下载的jar包放到 tomcat/webapps/ROOT/WEB-INF/lib/ 下
1. 重新启动运行项目，登陆网站管理后台，即可看到左侧的 功能插件 下多了此功能。


## 二次开发
#### 本插件的二次开发
1. 运行项目，随便登陆个网站管理后台（默认运行起来后，会自带一个默认账号密码都是 wangzhan 的账号，可以用此账号登陆）
1. 登陆网站管理后台后，找到左侧的 功能插件 下的 表单反馈 ，即可。

#### 从头开始开发一个自己的插件
参考文档：  https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3213258&doc_id=1101390

#### 二次开发wangmarket及功能的扩展定制
可参考：  https://gitee.com/leimingyun/wangmarket_deploy

## 合作洽谈
作者：管雷鸣<br/>
微信：xnx3com<br/>
QQ：921153866<br/>
